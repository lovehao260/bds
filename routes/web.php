<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\OrganizationsController;
use App\Http\Controllers\PropertiesController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\WishlistController;
use App\Http\Controllers\HomesController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth


Route::get('login', [AuthenticatedSessionController::class, 'create'])
    ->name('login')
    ->middleware('guest');

Route::post('login', [AuthenticatedSessionController::class, 'store'])
    ->name('login.store')
    ->middleware('guest');
Route::get('/', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->middleware('auth');
Route::get('/search-home', [DashboardController::class, 'indexHome'])
    ->name('dashboard.home')
    ->middleware('auth');

Route::middleware(['auth'])->group(function () {
    Route::delete('logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('logout');

// Dashboard
    Route::get('/download/export', [ReportsController::class, 'export'])
        ->name('report.export');
    Route::get('/download/export/{from}/{to}/{address}', [ReportsController::class, 'download'])
        ->name('report.download');

    Route::get('reports', [ReportsController::class, 'index'])
        ->name('report')
        ->middleware('auth','isAdmin');
    Route::delete('activity/destroy/{id}', [ReportsController::class, 'destroy'])
        ->name('report.destroy')
        ->middleware('auth','isAdmin');
    Route::delete('activity/remove', [ReportsController::class, 'remove'])
        ->name('report.remove')
        ->middleware('auth','isAdmin');


// Users

    Route::get('users', [UsersController::class, 'index'])
        ->name('users')
        ->middleware('auth','isAdmin');

    Route::get('users/create', [UsersController::class, 'create'])
        ->name('users.create')
        ->middleware('auth','isAdmin');

    Route::post('users', [UsersController::class, 'store'])
        ->name('users.store')
        ->middleware('auth','isAdmin');

    Route::get('users/{user}/edit', [UsersController::class, 'edit'])
        ->name('users.edit')
        ->middleware('auth','isAdmin');

    Route::put('users/{user}', [UsersController::class, 'update'])
        ->name('users.update')
        ->middleware('auth','isAdmin');

    Route::delete('users/{user}', [UsersController::class, 'destroy'])
        ->name('users.destroy')
        ->middleware('auth','isAdmin');

    Route::put('users/{user}/restore', [UsersController::class, 'restore'])
        ->name('users.restore')
        ->middleware('auth','isAdmin');
// Users

    Route::get('customers', [CustomerController::class, 'index'])
        ->name('customers')
        ->middleware('auth');

    Route::get('customers/create', [CustomerController::class, 'create'])
        ->name('customers.create')
        ->middleware('auth');

    Route::post('customers', [CustomerController::class, 'store'])
        ->name('customers.store')
        ->middleware('auth');

    Route::get('customers/{customer}/edit', [CustomerController::class, 'edit'])
        ->name('customers.edit')
        ->middleware('auth');

    Route::put('customers/{customer}', [CustomerController::class, 'update'])
        ->name('customers.update')
        ->middleware('auth');

    Route::delete('customers/{customer}', [CustomerController::class, 'destroy'])
        ->name('customers.destroy')
        ->middleware('auth');

    Route::post('delete-customers', [CustomerController::class, 'destroyCheck'])
        ->name('customers.destroy')
        ->middleware('auth');

    Route::post('customers/get-info/{customer}', [CustomerController::class, 'getInfo'])
        ->name('customers.info')
        ->middleware('auth');

// Properties

    Route::get('properties', [PropertiesController::class, 'index'])
        ->name('properties')
        ->middleware('auth','isAdmin');

    Route::get('properties/create', [PropertiesController::class, 'create'])
        ->name('properties.create')
        ->middleware('auth','isAdmin');

    Route::post('properties', [PropertiesController::class, 'store'])
        ->name('properties.store')
        ->middleware('auth','isAdmin');

    Route::get('properties/{property}/edit', [PropertiesController::class, 'edit'])
        ->name('properties.edit')
        ->middleware('auth','isAdmin');

    Route::put('properties/{property}', [PropertiesController::class, 'update'])
        ->name('properties.update')
        ->middleware('auth','isAdmin');

    Route::post('delete-properties', [PropertiesController::class, 'destroyCheck'])
        ->name('properties.destroy')
        ->middleware('auth');


    Route::delete('properties/{property}', [PropertiesController::class, 'destroy'])
        ->name('properties.destroy')
        ->middleware('auth','isAdmin');

    Route::put('properties/{property}/restore', [PropertiesController::class, 'restore'])
        ->name('properties.restore')
        ->middleware('auth','isAdmin');

    Route::post('properties/uploadExcel', [PropertiesController::class, 'uploadExcel'])
        ->name('properties.upload')
        ->middleware('auth','isAdmin');

    Route::post('properties/upload-images/{property}', [PropertiesController::class, 'uploadImage'])
        ->name('properties.upload.image')
        ->middleware('auth');

    Route::post('properties/top/{property}', [PropertiesController::class, 'onTop'])
        ->name('properties.top')
        ->middleware('auth');

    Route::get('wishlists', [WishlistController::class,'getAllWishlistItems'])->name('wishlist.list');
    Route::post('wishlist/add/{property}', [WishlistController::class,'addToWishlist'])->name('wishlist.add');
    Route::delete('wishlist/remove/{property}', [WishlistController::class,'removeFromWishlist'])->name('wishlist.remove');


    Route::get('homes', [HomesController::class, 'index'])
        ->name('homes')
        ->middleware('auth','isAdmin');

    Route::get('homes/create', [HomesController::class, 'create'])
        ->name('homes.create')
        ->middleware('auth','isAdmin');

    Route::post('homes', [HomesController::class, 'store'])
        ->name('homes.store')
        ->middleware('auth','isAdmin');

    Route::get('homes/{home}/edit', [HomesController::class, 'edit'])
        ->name('homes.edit')
        ->middleware('auth','isAdmin');

    Route::put('homes/{home}', [HomesController::class, 'update'])
        ->name('homes.update')
        ->middleware('auth','isAdmin');

    Route::post('delete-homes', [HomesController::class, 'destroyCheck'])
        ->name('homes.destroy')
        ->middleware('auth');


    Route::delete('homes/{home}', [HomesController::class, 'destroy'])
        ->name('homes.destroy')
        ->middleware('auth','isAdmin');

    Route::put('homes/{home}/restore', [HomesController::class, 'restore'])
        ->name('homes.restore')
        ->middleware('auth','isAdmin');

    Route::post('homes/uploadExcel', [HomesController::class, 'uploadExcel'])
        ->name('homes.upload')
        ->middleware('auth','isAdmin');

    Route::post('homes/upload-images/{home}', [HomesController::class, 'uploadImage'])
        ->name('homes.upload.image')
        ->middleware('auth');

    Route::post('homes/top/{home}', [HomesController::class, 'onTop'])
        ->name('homes.top')
        ->middleware('auth');

});




