<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Contact;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $account = Account::create(['name' => 'Bất Động sản HCM']);

        User::factory()->create([
            'account_id' => $account->id,
            'first_name' => 'Hao',
            'last_name' => 'Le',
            'email' => 'haole042@gmail.com',
            'password' => 'admin123',
            'owner' => true,
        ]);

        User::factory(5)->create(['account_id' => $account->id]);


    }
}
