<?php
namespace App\Supports;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\LogActivity as LogActivityModel;
class Helper{
     static function  DefaultUrlImage($url, $home = false )
    {
        if ($url){
            if($home){
                return  Storage::url('home-images/'.$url );
            }
            return  Storage::url('images/'.$url );
        }else{
            return asset('images/no-image.jpg');
        }
    }

     static function addToLog($subject)
    {
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_id'] = auth()->check() ? auth()->user()->id : 1;
        LogActivityModel::create($log);
    }


     static function logActivityLists()
    {
        return LogActivityModel::with('user')->latest()->paginate( 15);
    }
}
