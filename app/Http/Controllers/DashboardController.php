<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\Home;
use App\Supports\Helper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index()
    {

        $number = Request::get('numberPaginate') ?? '12';
        $districtHcm = DB::table('district')->where('province_id', 50)
            ->pluck("name");

        $types = config("filesystems.types");

        $directions=  config('filesystems.direction');
        $query = Property::sortBy(Request::only('sortBy'))
            ->orderBy('updated_at', 'DESC')
            ->orderBy('id', 'DESC');
        $total = $query->count();
        $properties = $query->filterHome(Request::only('search', 'horizontal', 'vertical', 'type', 'district','direction'))
            ->price(Request::only('from', 'to'))
            ->filterDate(Request::only('from_date', 'to_date'))
            ->where('del_flg',0)
            ->with('images')
            ->paginate($number)
            ->withQueryString()
            ->through(fn($property) => [
                'id' => $property->id,
                'address' => $property->name,
                'phones' => preg_split("/,/", $property->phone),
                'horizontal' => $property->horizontal,
                'vertical' => $property->vertical,
                'price' => $property->price,
                'description' => $property->description,
                'status' => $property->status,
                'typename' => $property->typename,
                'date' => $property->date,
                'images' => $property->images,
                'direction' => $property->nameDirection,
                'image' => Helper::DefaultUrlImage($property->images ? $property->images->pluck('file_path')->first() : '')
            ]);

        return Inertia::render('Dashboard/Index', [
            'filters' => Request::all('search', 'district', 'from', 'to', 'acreage', 'showing', 'sortBy','numberPaginate','form_date','to_date'),
            'properties' => $properties,
            'pagination'=>$number,
            "districtHcm" => $districtHcm,
            "total" => $total,
            "types" => $types,
            "directions"=>$directions,
            "customers"=>Request::get('customers') ?? ''
        ]);
    }

    public function indexHome()
    {

        $number = Request::get('numberPaginate') ?? '12';
        $districtHcm = DB::table('district')->where('province_id', 50)
            ->pluck("name");

        $types = config("filesystems.types");

        $directions=  config('filesystems.direction');
        $query = Home::sortBy(Request::only('sortBy'))
            ->orderBy('updated_at', 'DESC')
            ->orderBy('id', 'DESC');
        $total = $query->count();
        $properties = $query->filterHome(Request::only('search', 'horizontal', 'vertical', 'type', 'district','direction'))
            ->price(Request::only('from', 'to'))
            ->filterDate(Request::only('from_date', 'to_date'))
            ->where('del_flg',0)
            ->with('images')
            ->paginate($number)
            ->withQueryString()
            ->through(fn($property) => [
                'id' => $property->id,
                'address' => $property->name,
                'phones' => preg_split("/,/", $property->phone),
                'horizontal' => $property->horizontal,
                'vertical' => $property->vertical,
                'price' => $property->price,
                'description' => $property->description,
                'status' => $property->status,
                'typename' => $property->typename,
                'date' => $property->date,
                'images' => $property->images,
                'direction' => $property->nameDirection,
                'image' => Helper::DefaultUrlImage($property->images ? $property->images->pluck('file_path',)->first() : '', true)
            ]);

        return Inertia::render('Dashboard/Home', [
            'filters' => Request::all('search', 'district', 'from', 'to', 'acreage', 'showing', 'sortBy','numberPaginate','form_date','to_date'),
            'properties' => $properties,
            'pagination'=>$number,
            "districtHcm" => $districtHcm,
            "total" => $total,
            "types" => $types,
            "directions"=>$directions,
            "customers"=>Request::get('customers') ?? ''
        ]);
    }
}
