<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\WishlistItem;
use App\Supports\Helper;
use Illuminate\Http\Request;
use Inertia\Inertia;

class WishlistController extends Controller
{
    public function getAllWishlistItems(Request $request)
    {
        $user = auth()->user();

        $properties= $user->wishlistProperties()->when($request->search ?? null, function ($query, $search) {
            $query->where('description', 'like', '%' . $search . '%');
            $query->orWhere('address', 'like', '%' . $search . '%');
            $query->orWhere('phone', 'like', '%' . $search . '%');
        })->paginate(15)
            ->withQueryString()
            ->through(fn($property) => [
                'id' => $property->id,
                'address' => $property->name,
                'phones' => preg_split("/,/", $property->phone),
                'horizontal' => $property->horizontal,
                'vertical' => $property->vertical,
                'price' => $property->price,
                'description' => $property->description,
                'status' => $property->status,
                'typename' => $property->typename,
                'date' => $property->date,
                'images' => $property->images,
                'direction' => $property->nameDirection,
                'image' => Helper::DefaultUrlImage($property->images ? $property->images->pluck('file_path')->first() : '')
            ]);

        return Inertia::render('Dashboard/WishList', [
            'filters' => \Illuminate\Support\Facades\Request::all('search', 'status'),
            'properties' => $properties,

        ]);
    }


    public function addToWishlist(Property $property)
    {
        $user = auth()->user();

        if (!$user->wishlistItems()->where('property_id', $property->id)->exists()) {
            // Add the product to the user's wishlist
            WishlistItem::create([
                'user_id' => $user->id,
                'property_id' => $property->id,
            ]);

            return  response()->json(['msg'=>'Dự án đã được lưu vào danh dách yêu thích','error'=>false]);
        }
        auth()->user()->wishlistItems()->where('property_id', $property->id)->delete();
        return  response()->json(['msg'=>'Đã xóa dự án trong danh sách yêu thích','error'=>true]);
    }


    public function removeFromWishlist(Property $property)
    {
        auth()->user()->wishlistItems()->where('property_id', $property->id)->delete();
        return redirect()->back()->with('success', 'Đã xóa dự án trong danh sách yêu thích.');
    }
}
