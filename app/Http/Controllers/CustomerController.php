<?php

namespace App\Http\Controllers;


use App\Models\Customer;

use App\Models\User;
use App\Supports\Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request as RequestA;
use Inertia\Inertia;


class CustomerController  extends Controller
{
    public function index()
    {
        $number = Request::get('numberPaginate') ?? 10;
        $query = Customer::where('del_flg', 0)->orderBy('updated_at','DESC')->orderBy('id','DESC');
        if (Auth::user()['owner'] < 2){
            $query->where('created', Auth::id())->orWhere("of_user", Auth::id())->where('del_flg', 0);
        }
        $total = $query->count();
        $customers = $query->filter(Request::only('search','district','sale','status'))
            ->price(Request::only('from_price', 'to_price'))
            ->with('user')
            ->filterDate(Request::only('from_date', 'to_date'))
            ->paginate( $number)
            ->withQueryString()
            ->through(fn($customer) => [
                'id' => $customer->id,
                'name' => $customer->name,
                'phone' => $customer->phone,
                'district' => $customer->district,
                'price' => $customer->price,
                'date' => $customer->date ? date('Y-m-d', strtotime($customer->date)) : '',
                'description' => $customer->description,
                'status'=> $customer->status,
                'note'=>$customer->note,
                'user' => $customer->user
            ]);

        $pagination = config('filesystems.pagination');
        $status=  config('filesystems.status');
        $sales = User::select(DB::raw('CONCAT_WS(" ", `first_name`, `last_name`) AS full_name'), 'id')
            ->pluck('full_name', 'id');
        $districtHcm = DB::table('district')->where('province_id', 50)
            ->pluck("name");

        return Inertia::render('Customer/Index', [
            'filters' => Request::all('search','district','sale','status','from_price','to_price',"from_date",'to_Date'),
            'customers' => $customers,
            'pagination'=>$pagination,
            'districtHcm'=>$districtHcm,
            'status'=>$status,
            'sales'=>$sales,
            'total'=>$total
        ]);
    }

    public function create()
    {
        $districtHcm = DB::table('district')->where('province_id', 50)
            ->pluck("name");
        $users = User::select(DB::raw('CONCAT_WS(" ", `first_name`, `last_name`) AS full_name'), 'id')
            ->pluck('full_name', 'id');
        $directions=  config('filesystems.direction');
        $types = config("filesystems.types");
        $status=  config('filesystems.status');
        return Inertia::render('Customer/Create',[
            "districtHcm"=>$districtHcm,
            "directions"=>$directions,
            "types"=>$types,
            "status"=>$status,
            'users'=>$users
        ]);
    }


    public function store()
    {

        Request::validate([
//            'address' => ['required', 'max:150'],
            'customer' => ['required'],
            'horizontal' => ["nullable"],
            'vertical' => ['nullable'],
            'phones' => ['required'],
            'price' => ['nullable','numeric'],
            'date' => ['nullable', 'date'],
        ]);

        DB::transaction(function ()   {
            Customer::create([
                'name' => Request::get('customer'),
                'district' => Request::get('district')?  implode(",",Request::get('district')) : null,
                'price' => str_replace(',', '', Request::get('price')),
                'phone' => Request::get('phones'),
                'type' => Request::get('type'),
                'date' => Request::get('date'),
                'horizontal' => Request::get('horizontal'),
                'vertical' => Request::get('vertical'),
                'description' => Request::get('description'),
                'direction' => Request::get('direction'),
                'status' => Request::get('status'),
                'note' => Request::get('note'),
                'created' => Auth::id(),
                'of_user' => Request::get('of_user'),
            ]);

            Helper::addToLog("Tạo mới một customer");
        });

        return Redirect::route('customers')->with('success', 'Thêm khách hàng thành công.');
    }

    public function edit(Customer $customer)
    {
        $districtHcm= DB::table('district')->where('province_id',50)->pluck("name");
        $directions=  config('filesystems.direction');
        $types = config("filesystems.types");
        $status=  config('filesystems.status');
        $users = User::select(DB::raw('CONCAT_WS(" ", `first_name`, `last_name`) AS full_name'), 'id')
            ->pluck('full_name', 'id');

        return Inertia::render('Customer/Edit', [
            'property' => [
                'id' => $customer->id,
                'name' => $customer->name,
                'type' => $customer->type,
                'horizontal' =>$customer->horizontal,
                'vertical' =>$customer->vertical,
                'price' => $customer->price,
                'district' => $customer->district ? explode (",",$customer->district):[],
                'phone' =>  $customer->phone ,
                'description' => $customer->description,
                'direction' => $customer->direction,
                'status' => $customer->status,
                'date' => $customer->date,
                'note'=>$customer->note,
                'of_user' => $customer->of_user,
            ],
            "districtHcm"=>$districtHcm,
            "types"=>$types,
            "directions"=>$directions,
            "status"=>$status,
            "users"=>$users
        ]);

    }

    public function update(Customer $customer)
    {
        Request::validate([
//            'address' => ['required', 'max:150'],
            'name' => ['required'],
            'horizontal' => ["nullable"],
            'vertical' => ['nullable'],
            'phones' => ['required'],
            'price' => ['nullable','numeric'],
            'date' => ['nullable', 'date'],

        ]);

        DB::transaction(function () use ($customer) {
            $customer->update([
                'name' => Request::get('name'),
                'district' =>  implode(",",Request::get('district')),
                'price' => str_replace(',', '', Request::get('price')),
                'phone' => Request::get('phones'),
                'type' => Request::get('type'),
                'date' => Request::get('date'),
                'horizontal' => Request::get('horizontal'),
                'vertical' => Request::get('vertical'),
                'description' => Request::get('description'),
                'direction' => Request::get('direction'),
                'status' => Request::get('status'),
                'note' => Request::get('note'),
                'of_user' => Request::get('of_user'),
            ]);

        });
        Helper::addToLog("Cập nhật customer $customer->name");
        return Redirect::route('customers')->with('success', 'sửa đổi thành công.');
    }


    public function destroy (Customer $customer){

        Helper::addToLog("Xoá customer $customer->name");
        $customer->update([
            'del_flg' => 1
        ]);

        return Redirect::back()->with('success', 'Xóa thành công');
    }

    public function destroyCheck(RequestA $request){
        if ($request->ids){
            foreach ($request->ids as $id){
                $customer = Customer::find($id);
                $customer->update([
                    'del_flg' => 1,
                    'updated_at' => now()
                ]);
                Helper::addToLog("Xoá customer $customer->name");
            }
        }
        if($request->all){
            Customer::where('del_flg', 0)->update(['del_flg' => 1]);
            Helper::addToLog("Xoá tất cả customer ");
        }

        return Redirect::back()->with('success', 'Xóa thành công');
    }

    public function getInfo(Customer $customer){
        return response()->json($customer);
    }
}
