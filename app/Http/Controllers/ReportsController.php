<?php

namespace App\Http\Controllers;

use App\Exports\PropertyExport;
use App\Models\Image;
use App\Models\LogActivity;
use App\Models\Property;
use App\Models\User;
use App\Supports\Helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    public function index()
    {

        $total = Property::get()->count();
        $totalImages = Image::get()->count();
        $numberDay = Property::whereDate('created_at', Carbon::now())->count();

        $start_date_month = Carbon::now()->subDays(30);
        $start_date_week = Carbon::now()->subDays(7);
        $end_date = Carbon::now();

        $week = Property::whereBetween('created_at', [$start_date_week, $end_date])->count();
        $month = Property::whereBetween('created_at', [$start_date_month, $end_date])->count();


        $numberUser = User::get()->count();
        $districtHcm= DB::table('district')->where('province_id',50)->pluck("name");

        $logs = LogActivity::with('user')->latest()->paginate( 15);

        return Inertia::render('Report/Index',[
            "numberDay"=>$numberDay,
            "numberWeek"=>$week,
            "numberMonth"=>$month,
            "numberUser"=>$numberUser,
            "total"=>$total,
            "totalImages"=>$totalImages,
            "districtHcm"=>$districtHcm,
            "logs"=>$logs
        ]);
    }


    public function download($from, $to,$address){

        if ($from ==="null" || $to ==="null"){
            return Redirect::back()->with('error', 'Vui lòng nhập ngày bắt đầu và ngày kết thúc .');
        }
        $carbonDate1 = Carbon::parse($from);
        $carbonDate2 = Carbon::parse($to);
        if (!$carbonDate1->lessThan($carbonDate2)){
            return Redirect::back()->with('error', 'Ngày bắt đầu không lớn hơn kết thúc');
        }
        $name = 'properties_from_' . $carbonDate1->format('Y-m-d') .'_to_'. $carbonDate2->format('Y-m-d') . '.xlsx';
        return Excel::download(new PropertyExport($from,$to,$address), $name);


    }
    public function destroy($id){
        LogActivity::find($id)->delete();
        return Redirect::back()->with('success', 'Xoá lịch sử hoạt động thành công ');
    }
    public function remove(){
        LogActivity::query()->delete();
        return Redirect::back()->with('success', 'Xoá lịch sử hoạt động thành công ');
    }

}
