<?php

namespace App\Http\Controllers;

use App\Imports\PropertyImport;
use App\Models\HomeImage;
use App\Models\Home;
use App\Supports\Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request as RequestA;
use App\Helpers\ImageHelper;

class HomesController extends Controller
{


    public function index()
    {

        $number = Request::get('numberPaginate') ?? 10;
        $districtHcm = DB::table('district')->where('province_id', 50)
            ->pluck("name");
        $homes = Home::orderBy('updated_at','DESC')->orderBy('id','DESC')
            ->filter(Request::only('search', 'status','district'))
            ->where('del_flg',0)
            ->with('images')
            ->paginate( $number)
            ->withQueryString()
            ->through(fn($home) => [
                'id' => $home->id,
                'address' => $home->address,
                'phone' => $home->phone,
                'price' => $home->price,
                'status' => $home->status,
                'name' => $home->name,
                'image' => Helper::DefaultUrlImage($home->images ? $home->images->pluck('file_path')->first() : '', true)
            ]);

        $pagination = config('filesystems.pagination');

        return Inertia::render('Home/Index', [
            'filters' => Request::all('search', 'status','district'),
            'homes' => $homes,
            'pagination'=>$pagination,
            'districtHcm'=>$districtHcm
        ]);
    }

    public function create()
    {
        $districtHcm= DB::table('district')->where('province_id',50)->pluck("name","district_id");
        $directions=  config('filesystems.direction');
        $types = config("filesystems.types");
        return Inertia::render('Home/Create',[
            "districtHcm"=>$districtHcm,
            "types"=>$types,
            "directions"=>$directions
        ]);
    }


    public function store()
    {

        Request::validate([
//            'address' => ['required', 'max:150'],

            'horizontal' => ["nullable"],
            'vertical' => ['nullable'],
            'phones' => ['required'],
            'type' => ['required'],
            'status' => ['required', 'boolean'],
            'images.*' => ['nullable', 'image','max:5120'],
        ]);
        $address = DB::table('district')->where('district_id',Request::get('district'))->first() ?? "";
        DB::transaction(function () use ($address)  {
            $home = Home::create([
                'address' => $address->name,
                'district' => Request::get('district'),
                'price' => str_replace(',', '', Request::get('price')),
                'phone' => implode(",",Request::get('phones')),
                'type' => Request::get('type'),
                'horizontal' => Request::get('horizontal'),
                'vertical' => Request::get('vertical'),
                'status' => Request::get('status'),
                'description' => Request::get('description'),
                'direction' => Request::get('direction'),
                'updated_at' => now()
            ]);
            if (Request::file('images')) {
                foreach (Request::file('images') as $file) {
                    $fileName = ImageHelper::uploadWithWatermark($file, true);
                    HomeImage::create([
                        'home_id' => $home->id,
                        'file_path' => $fileName
                    ]);
                }
            }

            Helper::addToLog('Tạo mới một property');
        });

        return Redirect::route('homes')->with('success', 'Tạo mới một property Thành công.');
    }

    public function edit(Home $home)
    {

        $images = HomeImage::where('home_id',$home->id)
            ->with('user')
            ->get();

        $districtHcm= DB::table('district')->where('province_id',50)->pluck("name","district_id");
        $directions=  config('filesystems.direction');
        $types = config("filesystems.types");
        return Inertia::render('Home/Edit', [
            'home' => [
                'id' => $home->id,
                'type' => $home->type,
                'horizontal' =>$home->horizontal,
                'district' => $home->district,
                'vertical' =>$home->vertical,
                'price' => $home->price ,
                'address' => $home->address,
                'phone' =>  $home->phone ? explode (",",$home->phone):[],
                'status' => $home->status,
                'description' => $home->description,
                'direction' => $home->direction,
            ],
            'images'=>$images->toArray(),
            "districtHcm"=>$districtHcm,
            "types"=>$types,
            "directions"=>$directions
        ]);

    }

    public function update(Home $home)
    {

        $data = request()->validate([

            'horizontal' => ['nullable'],
            'vertical' => ['nullable'],
            'phones' => ['required', 'array'],
            'type' => ['required'],
            'status' => ['required', 'boolean'],
            'images.*' => ['nullable', 'image', 'max:5120'],
        ]);

        $address = DB::table('district')->where('district_id',Request::get('district'))->first() ?? "";

        DB::transaction(function () use ($home, $address, $data) {
            // Update property data
            $home->update([
                'address' => $address->name,
                'district' => Request::get('district'),
                'price' => Request::get('price'),
                'phone' => implode(",",Request::get('phones')),
                'type' => Request::get('type'),
                'horizontal' => Request::get('horizontal'),
                'vertical' => Request::get('vertical'),
                'status' => Request::get('status'),
                'description' => Request::get('description'),
                'direction' => Request::get('direction'),
                'updated_at' => now()
            ]);

            // Handle old images deletion
            if (request('imagesOld')) {
                foreach (request('imagesOld') as $fileOld) {
                    $file = HomeImage::find($fileOld);
                    if ($file) {
                        $file->deleteFile();  // Assumes deleteFile() removes the file
                        $file->delete();  // Removes the DB entry
                    }
                }
            }

            // Handle new image upload with watermark
            if (request()->hasFile('images')) {
                foreach (request()->file('images') as $file) {
                    $fileName = ImageHelper::uploadWithWatermark($file,true);

                    // Save the image path to the database
                    HomeImage::create([
                        'home_id' => $home->id,
                        'file_path' => $fileName,
                        'created_by' => auth()->id(),
                    ]);
                }
            }
        });

        // Log action and return with success message
        Helper::addToLog('Cập nhật một property');
        return Redirect::route('homes')->with('success', 'Sửa đổi thành công.');
    }

    public function uploadExcel(): \Illuminate\Http\RedirectResponse
    {
        if (Auth::user() &&  Auth::user()->owner < 2) {
            return redirect('/')->with('error', 'Bạn không có quyền thực hiện thao tác này.');
        }
        Request::validate([
            'excel.*' => ['required', 'file', 'mimes:xlsx,xls'],
        ]);
        foreach (Request::file('excel') as $excel){
            Excel::import(new PropertyImport, $excel);
        }
        try{

            return Redirect::route('homes')->with('success', 'Import thành công.');
        }catch (\Exception $exception ){
            return Redirect::route('homes')->with('errors', 'Import thất bại.');
        }
    }

    public function destroy (Home $home){
        if (Auth::user() &&  Auth::user()->owner < 2) {
            return redirect('/homes')->with('error', 'Bạn không có quyền thực hiện thao tác này.');
        }
        $images = HomeImage::where('home_id',$home->id)->get();

        foreach ($images as $fileOld) {
            $file = HomeImage::find($fileOld->id);
            $file->deleteFile();
            $file->delete();
        }
        $home->update([
            'del_flg' => 1
        ]);
        //$home->delete();
        Helper::addToLog('Xoá một property');
        return Redirect::back()->with('success', 'Xóa thành công');
    }

    public function destroyCheck(RequestA $request){
        if (Auth::user() &&  Auth::user()->owner < 2) {
            return redirect('/homes')->with('error', 'Bạn không có quyền thực hiện thao tác này.');
        }
        try{
            if ($request->ids){
                DB::beginTransaction();
                foreach ($request->ids as $id){
                    $home = Home::find($id);
                    $images = HomeImage::where('home_id',$id)->get();
                    foreach ($images as $fileOld) {
                        $file = HomeImage::find($fileOld->id);
                        $file->deleteFile();
                        $file->delete();
                    }
                    $home->update([
                        'del_flg' => 1
                    ]);
                    // $home->delete();
                    Helper::addToLog("Xoá một property $home->phone" );
                }
                DB::commit();
            }
            $mess = 'Xóa thành công';
        }catch (\Exception $exception){
            DB::rollBack();
            $mess = 'Xóa Thất bại';
        }

        return Redirect::back()->with('success',$mess);
    }
    public function uploadImage(Home $home){

        Request::validate([
            'images.*' => ['nullable', 'image','max:5120'],
        ]);
        if (Request::file('images')) {
            foreach (Request::file('images') as $file) {

                $fileName = ImageHelper::uploadWithWatermark($file, true);

                HomeImage::create([
                    'home_id' => $home->id,
                    'file_path' => $fileName,
                    'created_by' => auth()->id()
                ]);
            }
        }

        Helper::addToLog('Tải lên hình ảnh');
    }

    public function onTop(Home $home){
        $home->update([

            'updated_at' => now(),
        ]);
        return Redirect::route('homes')->with('success', 'Làm Mới thành công.');
    }
}
