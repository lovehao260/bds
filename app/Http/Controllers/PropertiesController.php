<?php

namespace App\Http\Controllers;

use App\Imports\PropertyImport;
use App\Models\Image;
use App\Models\Property;
use App\Supports\Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request as RequestA;
use App\Helpers\ImageHelper;
class PropertiesController extends Controller
{


    public function index()
    {

        $number = Request::get('numberPaginate') ?? 10;
        $districtHcm = DB::table('district')->where('province_id', 50)
            ->pluck("name");
        $properties = Property::orderBy('updated_at','DESC')->orderBy('id','DESC')
            ->filter(Request::only('search', 'status','district'))
            ->where('del_flg',0)
            ->with('images')
            ->paginate( $number)
            ->withQueryString()
            ->through(fn($property) => [
                'id' => $property->id,
                'address' => $property->address,
                'phone' => $property->phone,
                'price' => $property->price,
                'status' => $property->status,
                'name' => $property->name,
                'image' => Helper::DefaultUrlImage($property->images ? $property->images->pluck('file_path')->first() : '')
            ]);

        $pagination = config('filesystems.pagination');

        return Inertia::render('Properties/Index', [
            'filters' => Request::all('search', 'status','district'),
            'properties' => $properties,
            'pagination'=>$pagination,
            'districtHcm'=>$districtHcm
        ]);
    }

    public function create()
    {
        $districtHcm= DB::table('district')->where('province_id',50)->pluck("name","district_id");
        $directions=  config('filesystems.direction');
        $types = config("filesystems.types");
        return Inertia::render('Properties/Create',[
            "districtHcm"=>$districtHcm,
            "types"=>$types,
            "directions"=>$directions
        ]);
    }


    public function store()
    {

        Request::validate([
//            'address' => ['required', 'max:150'],
            'horizontal' => ["nullable"],
            'vertical' => ['nullable'],
            'phones' => ['required'],
            'type' => ['required'],
            'status' => ['required', 'boolean'],
            'images.*' => ['nullable', 'image','max:5120'],
        ]);
        $address = DB::table('district')->where('district_id',Request::get('district'))->first() ?? "";
        DB::transaction(function () use ($address)  {
            $property = Property::create([
                'address' => $address->name,
                'district' => Request::get('district'),
                'price' => str_replace(',', '', Request::get('price')),
                'phone' => implode(",",Request::get('phones')),
                'type' => Request::get('type'),
                'horizontal' => Request::get('horizontal'),
                'vertical' => Request::get('vertical'),
                'status' => Request::get('status'),
                'description' => Request::get('description'),
                'direction' => Request::get('direction'),
                'updated_at' => now()
            ]);
            if (Request::file('images')) {
                foreach (Request::file('images') as $file) {
                    $fileName = ImageHelper::uploadWithWatermark($file);
                    Image::create([
                        'property_id' => $property->id,
                        'file_path' => $fileName
                    ]);
                }
            }

            Helper::addToLog('Tạo mới một property');
        });

        return Redirect::route('properties')->with('success', 'Tạo mới một property Thành công.');
    }

    public function edit(Property $property)
    {
        $images = Image::where('property_id',$property->id)
            ->with('user')
            ->get();

        $districtHcm= DB::table('district')->where('province_id',50)->pluck("name","district_id");
        $directions=  config('filesystems.direction');
        $types = config("filesystems.types");
        return Inertia::render('Properties/Edit', [
            'property' => [
                'id' => $property->id,
                'type' => $property->type,
                'horizontal' =>$property->horizontal,
                'district' => $property->district,
                'vertical' =>$property->vertical,
                'price' => $property->price,
                'address' => $property->address,
                'phone' =>  $property->phone ? explode (",",$property->phone):[],
                'status' => $property->status,
                'description' => $property->description,
                'direction' => $property->direction,
            ],
            'images'=>$images->toArray(),
            "districtHcm"=>$districtHcm,
            "types"=>$types,
            "directions"=>$directions
        ]);

    }

    public function update(Property $property)
    {

        $data = request()->validate([
            'horizontal' => ['nullable'],
            'vertical' => ['nullable'],
            'phones' => ['required', 'array'],
            'type' => ['required'],
            'status' => ['required', 'boolean'],
            'images.*' => ['nullable', 'image', 'max:5120'],
        ]);

        $address = DB::table('district')->where('district_id',Request::get('district'))->first() ?? "";

        DB::transaction(function () use ($property, $address, $data) {
            // Update property data
            $property->update([
                'address' => $address->name,
                'district' => Request::get('district'),
                'price' => str_replace(',', '', Request::get('price')),
                'phone' => implode(",",Request::get('phones')),
                'type' => Request::get('type'),
                'horizontal' => Request::get('horizontal'),
                'vertical' => Request::get('vertical'),
                'status' => Request::get('status'),
                'description' => Request::get('description'),
                'direction' => Request::get('direction'),
                'updated_at' => now()
            ]);

            // Handle old images deletion
            if (request('imagesOld')) {
                foreach (request('imagesOld') as $fileOld) {
                    $file = Image::find($fileOld);
                    if ($file) {
                        $file->deleteFile();  // Assumes deleteFile() removes the file
                        $file->delete();  // Removes the DB entry
                    }
                }
            }

            // Handle new image upload with watermark
            if (request()->hasFile('images')) {
                foreach (request()->file('images') as $file) {
                    $fileName = ImageHelper::uploadWithWatermark($file);

                    // Save the image path to the database
                    Image::create([
                        'property_id' => $property->id,
                        'file_path' => $fileName,
                        'created_by' => auth()->id(),
                    ]);
                }
            }
        });

        // Log action and return with success message
        Helper::addToLog('Cập nhật một property');
        return Redirect::route('properties')->with('success', 'Sửa đổi thành công.');
    }

    public function uploadExcel(): \Illuminate\Http\RedirectResponse
    {
        if (Auth::user() &&  Auth::user()->owner < 2) {
            return redirect('/')->with('error', 'Bạn không có quyền thực hiện thao tác này.');
        }
        Request::validate([
            'excel.*' => ['required', 'file', 'mimes:xlsx,xls'],
        ]);
        foreach (Request::file('excel') as $excel){
            Excel::import(new PropertyImport, $excel);
        }
        try{

            return Redirect::route('properties')->with('success', 'Import thành công.');
        }catch (\Exception $exception ){
            return Redirect::route('properties')->with('errors', 'Import thất bại.');
        }
    }

    public function destroy (Property $property){
        if (Auth::user() &&  Auth::user()->owner < 2) {
            return redirect('/properties')->with('error', 'Bạn không có quyền thực hiện thao tác này.');
        }
        $images = Image::where('property_id',$property->id)->get();

        foreach ($images as $fileOld) {
            $file = Image::find($fileOld->id);
            $file->deleteFile();
            $file->delete();
        }
        $property->update([
            'del_flg' => 1
        ]);
        //$property->delete();
        Helper::addToLog('Xoá một property');
        return Redirect::back()->with('success', 'Xóa thành công');
    }

    public function destroyCheck(RequestA $request){
        if (Auth::user() &&  Auth::user()->owner < 2) {
            return redirect('/properties')->with('error', 'Bạn không có quyền thực hiện thao tác này.');
        }
        try{
            if ($request->ids){
                DB::beginTransaction();
                foreach ($request->ids as $id){
                    $property = Property::find($id);
                    $images = Image::where('property_id',$id)->get();
                    foreach ($images as $fileOld) {
                        $file = Image::find($fileOld->id);
                        $file->deleteFile();
                        $file->delete();
                    }
                    $property->update([
                        'del_flg' => 1
                    ]);
                    // $property->delete();
                    Helper::addToLog("Xoá một property $property->phone" );
                }
                DB::commit();
            }
            $mess = 'Xóa thành công';
        }catch (\Exception $exception){
            DB::rollBack();
            $mess = 'Xóa Thất bại';
        }

        return Redirect::back()->with('success',$mess);
    }
    public function uploadImage(Property $property){

        Request::validate([
            'images.*' => ['nullable', 'image','max:5120'],
        ]);
        if (Request::file('images')) {
            foreach (Request::file('images') as $file) {

                $fileName = ImageHelper::uploadWithWatermark($file);

                Image::create([
                    'property_id' => $property->id,
                    'file_path' => $fileName,
                    'created_by' => auth()->id()
                ]);
            }
        }

        Helper::addToLog('Tải lên hình ảnh');
    }

    public function onTop(Property $property){
        $property->update([

            'updated_at' => now(),
        ]);
        return Redirect::route('properties')->with('success', 'Làm Mới thành công.');
    }
}
