<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Customer extends Model
{
    use HasFactory;
    const UPDATED_AT = null;

    protected $casts = [
        'price' => 'float',
    ];
    public function user()
    {
        return $this->hasOne(User::class,'id','of_user');
    }
    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('description', 'like', '%' . $search . '%');
            $query->orWhere('name', 'like', '%' . $search . '%');
            $query->orWhere('district', 'like', '%' . $search . '%');
            $query->orWhere('phone', 'like', '%' . $search . '%');
        })
            ->when($filters['sale'] ?? null, function ($query, $sale) {
                $query->where('of_user', $sale );
            })
            ->when($filters['status'] ?? null, function ($query, $status) {
                $query->where('status', $status );
            })
            ->when($filters['district'] ?? null, function ($query, $districts) {
            if (is_array($districts)) {
                $query->where(function ($query) use ($districts) {
                    foreach ($districts as $district) {
                        $new_string = $district;
                        if (!ctype_digit($district)) {
                            $string = strtolower($district);
                            $new_string = str_replace('quận ', '', $string);
                        }
                        $query->orWhere('district', 'like', '%' . $new_string . '%');

                    }
                });

            } else {
                $query->where('district', 'like', '%' . $districts . '%');
            }
        });
    }
    public function scopePrice($query, array $filters)
    {
        $query->when($filters['from_price'] ?? null, function ($query, $from) {

            $query->where('price', '>=', (int)$from);
        })->when($filters['to_price'] ?? null, function ($query, $to) {

            $query->where('price', '<=', (int)$to);
        });
    }

    public function scopeFilterDate($query, array $filters)
    {
        $query->when(isset($filters['from_date']), function ($query) use ($filters) {
            $from = date('Y-m-d H:i:s', strtotime($filters['from_date']));
            $query->where('date', '>=', $from);
        })->when(isset($filters['to_date']), function ($query) use ($filters) {
            $to = date('Y-m-d H:i:s', strtotime($filters['to_date']));
            $query->where('date', '<=', $to);
        });
    }

}
