<?php

namespace App\Models;

use App\Supports\Helper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    use HasFactory;
    protected $table = 'images';
    public function property()
    {
        return $this->belongsTo(Property::class);
    }
    public function getUrlAttribute()
    {
        return asset('storage/images/'.$this->path);
    }
    public function user()
    {
        return $this->belongsTo(User::class,'created_by','id');
    }
    public function deleteFile()
    {
        Storage::delete("public/images/".$this->file_path);
        $this->delete();
    }
}
