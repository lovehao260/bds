<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Home extends Model
{
    use HasFactory;

    const UPDATED_AT = null;



    public function wishlist()
    {
        return $this->hasMany(WishlistItem::class);
    }

    public function images()
    {
        return $this->hasMany(HomeImage::class);
    }

    public function getDateAttribute()
    {
        return Carbon::parse($this->updated_at)->format('d/m/Y H:i:s');
    }

    public function getTypeNameAttribute()
    {
        $indexOfKey = array_search($this->type, array_keys(config("filesystems.types")));
        if ($indexOfKey !== false) {
            return config("filesystems.types")[$this->type];
        }
        return $this->type;
    }

    public function getNameAttribute()
    {
        $string = $this->description;


        $position = strpos(strtolower($string), strtolower($this->address));

        if ($position !== false) {
            $slicedString = substr(strtolower($string), 0, $position);
            return $slicedString . " " . $this->address;
        }
        return $this->address;
    }

    public function getNameDirectionAttribute()
    {

        $name = config('filesystems.direction.' . $this->direction);
        return $name ?? '';
    }

    public function scopeFilterDate($query, array $filters)
    {
        $query->when(isset($filters['from_date']), function ($query) use ($filters) {
            $from = date('Y-m-d H:i:s', strtotime($filters['from_date']));
            $query->where('created_at', '>=', $from);
        })->when(isset($filters['to_date']), function ($query) use ($filters) {
            $to = date('Y-m-d H:i:s', strtotime($filters['to_date']));
            $query->where('created_at', '<=', $to);
        });
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('description', 'like', '%' . $search . '%');
            $query->orWhere('address', 'like', '%' . $search . '%');
            $query->orWhere('phone', 'like', '%' . $search . '%');
        })->when($filters['district'] ?? null, function ($query, $districts) {
            if (is_array($districts)) {
                $query->where(function ($query) use ($districts) {
                    foreach ($districts as $district) {
                        if ($district === 'Quận 1') {
                            $query->orWhere('district', "550");
                        } else {
                            $new_string = $district;
                            if (!ctype_digit($district)) {
                                $string = strtolower($district);
                                $new_string = str_replace('quận ', '', $string);
                            }
                            $query->orWhere('address', 'like', '%' . $new_string . '%');
                        }

                    }
                });

            } else {
                $query->where('address', 'like', '%' . $districts . '%');
            }
        })->when($filters['status'] ?? null, function ($query, $status) {
            $query->orWhere('status', 'like', '%' . $status . '%');
        });
    }

    public function scopeFilterHome($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('description', 'like', '%' . $search . '%');
                $query->orWhere('address', 'like', '%' . $search . '%');
                $query->orWhere('phone', 'like', '%' . $search . '%');
            });

        })->when($filters['district'] ?? null, function ($query, $districts) {
            if (is_array($districts)) {
                $query->where(function ($query) use ($districts) {
                    foreach ($districts as $district) {
                        if ($district === 'Quận 1') {
                            $query->orWhere('district', "550");
                        } else {
                            $new_string = $district;
                            if (!ctype_digit($district)) {
                                $string = strtolower($district);
                                $new_string = str_replace('quận ', '', $string);
                            }
                            $query->orWhere('address', 'like', '%' . $new_string . '%');
                        }

                    }
                });

            } else {
                $query->where('address', 'like', '%' . $districts . '%');
            }
        })->when($filters['type'] ?? null, function ($query, $type) {
            $query->where('type', $type);
        })->when($filters['horizontal'] ?? null, function ($query, $horizontal) {
            $query->where('horizontal', '>=', (int)$horizontal);
        })->when($filters['vertical'] ?? null, function ($query, $vertical) {
            $query->where('vertical', '>=', (int)$vertical);
        })->when($filters['direction'] ?? null, function ($query, $direction) {
            $query->where('direction', $direction);
        });

    }

    public function scopePrice($query, array $filters)
    {
        $query->when($filters['from'] ?? null, function ($query, $from) {

            $query->where('price', '>=', (int)$from);
        })->when($filters['to'] ?? null, function ($query, $to) {

            $query->where('price', '<=', (int)$to);
        });
    }

    public function scopeSortBy($query, array $filters)
    {
        if ($filters['sortBy'] ?? null) {
            $query->when($filters['sortBy'] == "date_asc" ?? null, function ($query) {
                $query->orderBy('created_at', "ASC");
            })->when($filters['sortBy'] == "date_desc" ?? null, function ($query) {
                $query->orderBy('created_at', "DESC");
            })->when($filters['sortBy'] == "price_asc" ?? null, function ($query) {
                $query->orderByRaw('CAST(price AS FLOAT) ASC');
            })->when($filters['sortBy'] == "price_desc" ?? null, function ($query) {
                $query->orderByRaw('CAST(price AS FLOAT) DESC');
            })->when($filters['sortBy'] == "name_asc" ?? null, function ($query) {
                $query->orderBy('description', "ASC");
            })->when($filters['sortBy'] == "name_desc" ?? null, function ($query) {
                $query->orderBy('description', "DESC");
            });
        }

    }
}
