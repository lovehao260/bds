<?php

namespace App\Providers;

use App\Models\User;
use App\Supports\Helper;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Event::listen(\Illuminate\Auth\Events\Login::class, function ($event) {
            Helper::addToLog('Đăng nhập');
        });
        Event::listen(\Illuminate\Auth\Events\Logout::class, function ($event) {
            Helper::addToLog('Đăng Xuất');
        });
    }
}
