<?php

namespace App\Imports;

use App\Models\Property;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;


class PropertyImport implements ToModel, WithStartRow, WithBatchInserts, WithChunkReading
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        if (!isset($row[0])) {
            return null;
        }
        $data = DB::table('properties')->where('description', $row['1']);
        if ($data) {
            $data->update([
                    "horizontal" => $row[2],
                    "vertical" => $row[3],
                    "type" => $row[4],
                ]
            );
        } else {
            return new Property([
                'address' => $row[0],
                'description' => $row[1],
                'horizontal' => $row[2],
                'vertical' => $row[3],
                'type' => $row[4],
                'price' => $row[5],
                'phone' => $row[6],
            ]);
        }
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
