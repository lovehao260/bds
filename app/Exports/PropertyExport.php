<?php

namespace App\Exports;

use App\Models\Property;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class PropertyExport implements FromCollection, WithMapping,WithHeadings,WithColumnWidths
{

    protected string $startDate;
    protected string $endDate;
    protected string $address;
    public function __construct(string $startDate, string $endDate, string $address)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->address = $address;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $query =Property::whereBetween('created_at', [$this->startDate,  $this->endDate]);
        if($this->address !== 'null'){
            $districts =explode(',', $this->address );

            $query->where(function($query) use ( $districts ){
                foreach ( $districts as $district){
                    $new_string =$district;
                    if (!ctype_digit($district)) {
                        $string = strtolower($district);
                        $new_string = str_replace('quận ', '', $string);
                    }
                    $query->orWhere('address', 'like', '%' .$new_string . '%');
                }
            });
        }
        return $query->orderBy('address',"DESC")->get();
    }

    public function map($property): array
    {
            $hz =    $property->horizontal ?? '   ';

        return [
            $property->address,
            $property->description,
            (string)  $hz ,
            (string)  $property->vertical ,
            (string)  $property->typeName ?? 0,
            (string)  $property->price ?? 0,
            $property->phone,
        ];
    }

    public function headings(): array
    {
        return [ "Địa chỉ", "Mô tả", "Ngang", "Dài", "Loại Nhà", "Giá", "Số điện thoại"];
    }
    public function columnWidths(): array
    {
        return [
            'A' => 25,
            'B' => 80,
            'C' => 15,
            'D' => 15,
            'E' => 15,
            'F' => 15,
            'G' => 25,
        ];
    }
}
