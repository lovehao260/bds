<?php

namespace App\Helpers;

use Intervention\Image\Facades\Image as InterventionImage;
use Illuminate\Support\Facades\Storage;

class ImageHelper
{
    /**
     * Upload a file and apply a watermark
     *
     * @param \Illuminate\Http\UploadedFile $file The uploaded file object
     * @return string The file name of the uploaded image
     * @throws \Exception
     */
    public static function uploadWithWatermark($file, $home = false)
    {
        if ($file->isValid()) {
            $fileName = uniqid() . '-' . preg_replace('/\s+/', '-', $file->getClientOriginalName());
            $watermarkPath = public_path('images/logo_home_2.png');

            // Load the uploaded image
            $img = InterventionImage::make($file->getRealPath());

            // Load and resize the watermark
            $watermark = InterventionImage::make($watermarkPath);

            $imageWidth = $img->width();
            $imageHeight = $img->height();

            // Set watermark dimensions based on image width and height (doubling the current size)
            if ($imageWidth > 2000 || $imageHeight > 2000) {
                $watermarkWidth = round($imageWidth * 0.25);  // 10% doubled to 20%
                $watermarkHeight = round($imageHeight * 0.25);
            } elseif ($imageWidth > 1500 || $imageHeight > 1500) {
                $watermarkWidth = round($imageWidth * 0.2);  // 10% doubled to 20%
                $watermarkHeight = round($imageHeight * 0.2);
            } elseif ($imageWidth > 1000 || $imageHeight > 1000) {
                $watermarkWidth = round($imageWidth * 0.16); // 6% doubled to 12%
                $watermarkHeight = round($imageHeight * 0.16);
            } elseif ($imageWidth > 500 || $imageHeight > 500) {
                $watermarkWidth = round($imageWidth * 0.1); // 4% doubled to 8%
                $watermarkHeight = round($imageHeight * 0.1);
            } else {
                $watermarkWidth = round($imageWidth * 0.08); // 3% doubled to 6%
                $watermarkHeight = round($imageHeight * 0.08);
            }

            // Resize the watermark to 1/20 of both width and height, maintaining the aspect ratio
            $watermark->resize($watermarkWidth, $watermarkHeight, function ($constraint) {
                $constraint->aspectRatio();
            })->opacity(70);

            // Position the watermark at the center of the image
            $img->insert($watermark, 'center');

            // Save the watermarked image to the desired location
            $savePath = $home ? 'home-images/' : 'images/';
            $img->save(storage_path('app/public/' . $savePath . $fileName));

            return $fileName;
        }

        throw new \Exception('Invalid file uploaded.');
    }

}
